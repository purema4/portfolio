export default class Parallax {

    /**
     * Construct a class that will bind widow with scroll event in order to gen a parallax
     * @param {Object []} $elem list of all the element that needs to be parallaxed
     */
    constructor($elem=[]) {
        //debugger

        this.$elems = $elem;

        //setup the request animation frame
        window.requestAnimationFrame =  window.requestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.webkitRequestAnimationFrame
        || window.msRequestAnimationFrame
        || function(f){setTimeout(f, 1000/60)}

        this.bindScrollToWindow(this.parallaxclouds.bind(this))
    }

    /**
     * logic of the parallax here
     */    
    parallaxclouds() {
        let scrolltop = window.pageYOffset // get number of pixels document has scrolled vertically 
        this.$elems.forEach($elem => {
            $elem.style.transform = `translateY(${-scrolltop * $elem.dataset.parallax}px)`
        })
    }

    /**
     * Add scroll event to document.window
     * @param {Object} callback
     */    
    bindScrollToWindow(callback) {
        window.addEventListener('scroll', _ => {
            window.requestAnimationFrame(callback)
        })
    }
}